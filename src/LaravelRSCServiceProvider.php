<?php

namespace Sonmez\LaravelRSC;
use Illuminate\Support\ServiceProvider;

class LaravelRSCServiceProvider extends ServiceProvider
{

    public function boot(){
        if ($this->app->runningInConsole()) {
            $this->commands([
                //Package Commands
                RepoServicesPublishCommand::class,
                CreateRepoCommand::class
            ]);
        }
    }


    public function register()
    {

    }
}
