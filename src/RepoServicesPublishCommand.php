<?php

namespace Sonmez\LaravelRSC;

use Illuminate\Console\Command;
use Illuminate\Console\ConfirmableTrait;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\File;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\Finder\Finder;

class RepoServicesPublishCommand extends Command
{
    protected $signature = 'laravel-rsc:publish {--force : Overwrite any existing files}';

    protected $description = 'Publish all Base Services And Repos that are available for customization';

    use ConfirmableTrait;

    public function handle()
    {
        if (! $this->confirmToProceed()) {
            return 1;
        }
        $this->publishRepositories();
        $this->info('Publishing Repositories Done!');

        $this->publishServices();
        $this->info('Publishing Services Done!');

        $this->publishProviders();
        $this->info('Publishing Providers Done!');

        $this->info('All done!');
    }

    private function publishRepositories(){
        if (! is_dir($RepoPath =app_path('Repositories'))) {
            (new Filesystem)->makeDirectory($RepoPath);
        }
        if (! is_dir($RepoContractsPath =app_path('Repositories/Contracts'))) {
            (new Filesystem)->makeDirectory($RepoContractsPath);
        }

        collect(File::files(__DIR__ . '/../Repositories'))->each(function (SplFileInfo $file) use ($RepoPath) {
            $sourcePath = $file->getPathname();

            $targetPath = $RepoPath . "/{$file->getFilename()}";

            if (! file_exists($targetPath) || $this->option('force')) {
                file_put_contents($targetPath, file_get_contents($sourcePath));
            }
        });
        collect(File::files(__DIR__ . '/../Repositories/Contracts'))->each(function (SplFileInfo $file) use ($RepoContractsPath) {
            $sourcePath = $file->getPathname();

            $targetPath = $RepoContractsPath . "/{$file->getFilename()}";

            if (! file_exists($targetPath) || $this->option('force')) {
                file_put_contents($targetPath, file_get_contents($sourcePath));
            }
        });
    }

    private function publishServices(){
        if (! is_dir($ServicesPath =app_path('Services'))) {
            (new Filesystem)->makeDirectory($ServicesPath);
        }
        if (! is_dir($ServicesContractsPath =app_path('Services/Contracts'))) {
            (new Filesystem)->makeDirectory($ServicesContractsPath);
        }
        if (! is_dir($ServicesUserPath =app_path('Services/User'))) {
            (new Filesystem)->makeDirectory($ServicesUserPath);
        }

        collect(File::files(__DIR__ . '/../Services/Contracts'))->each(function (SplFileInfo $file) use ($ServicesContractsPath) {
            $sourcePath = $file->getPathname();

            $targetPath = $ServicesContractsPath . "/{$file->getFilename()}";

            if (! file_exists($targetPath) || $this->option('force')) {
                file_put_contents($targetPath, file_get_contents($sourcePath));
            }
        });
        collect(File::files(__DIR__ . '/../Services/User'))->each(function (SplFileInfo $file) use ($ServicesUserPath) {
            $sourcePath = $file->getPathname();

            $targetPath = $ServicesUserPath . "/{$file->getFilename()}";

            if (! file_exists($targetPath) || $this->option('force')) {
                file_put_contents($targetPath, file_get_contents($sourcePath));
            }
        });
    }

    private function publishProviders(){
        $ProvidersPath =app_path('Providers');
        collect(File::files(__DIR__ . '/../Providers'))->each(function (SplFileInfo $file) use ($ProvidersPath) {
            $sourcePath = $file->getPathname();

            $targetPath = $ProvidersPath . "/{$file->getFilename()}";

            if (! file_exists($targetPath) || $this->option('force')) {
                file_put_contents($targetPath, file_get_contents($sourcePath));
            }
        });
    }
}
