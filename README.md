#  Laravel Services and Repos

You can use this package create your repositories and services easily and you can customize them .


## Installation

1- You can install the package via composer:

```bash
composer require sonmez/laravel-rsc
```

2- After Successfull installation please run the following command :

```bash
php artisan laravel-rsc:publish
```

You will notice that there are two folder are created inside your app folder "Repositories" and "Services" along with two services providers to solve depenedency injection inside **Providers** Folder

3- Register ServiceLayerServiceProvider in `providers` section  your `config/app.php ` 

```bash
App\Providers\ServiceLayerServiceProvider::class,
```


## Usage

You can create a repository and service for your model by typing the following command: for example let us suppose that we want to do that for **Admin** Model that exists inside `App\Models`

```bash
php artisan create:repo Admin
```

##Support 
if you have any issue please post it in issuess section in this repo

## Security

If you discover any security related issues, please email lamasonmez@gmail instead of using the issue tracker.

